#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class Window_Info():

    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("./def_windows/window_main.ui")

        self.window_info = builder.get_object("window_info")

        btn_cerrar = builder.get_object("btn_fin")
        btn_cerrar.connect("clicked", self.on_btn_cerrar)

    def on_btn_cerrar(self, btn=None):
        self.window_info.destroy()
