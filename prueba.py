#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd

FILENAME = "suministro_alimentos_kcal.csv"
data = pd.read_csv(FILENAME)
data = data.fillna(0)

#funcion para modificar el csv
def modifica_data():
    new_data = data.drop(["Animal Products", "Animal fats", "Aquatic Products, Other" ,
                          "Cereals - Excluding Beer" ,"Eggs" ,"Fish, Seafood" ,"Fruits - Excluding Wine" ,
                          "Milk - Excluding Butter" ,"Miscellaneous" ,"Offals" ,"Oilcrops" ,"Pulses" ,
                          "Spices" ,"Starchy Roots" ,"Stimulants" ,"Sugar Crops" ,"Sugar & Sweeteners" ,
                          "Treenuts" ,"Vegetal Products" ,"Vegetable Oils" ,"Vegetables" ,"Undernourished" ,
                          "Recovered" ,"Active" ,"Unit (all except Population)"], axis = 1)
    return new_data

def clear_variables(fila):
    for i in range(170):
        if fila[i] > 100:
            fila[i] = 0
    return fila

#Pregunta 1: Asociacion entre consumo de carne y casos confirmados de covid
def pregunta_1(data):
    data_1 = data.loc[:, ["Country","Meat","Confirmed","Population"]]
    data_1["Confirmed"] = clear_variables(data_1["Confirmed"])
    data_1 = data_1.sort_values("Meat", ascending = False)
    data_1 = data_1.iloc[[0,1,2,3,4]]
    pais = data_1["Country"].to_list()
    carne = data_1["Meat"].to_list()
    data_1["Confirmed"] = (data_1["Population"]/100)*data_1["Confirmed"]
    confirmados = data_1["Confirmed"].to_list()
    poblacion = data_1["Population"].to_list()
    
    resp1 = (f"El pais que más consume carne con {int(carne[0])} Kcal es {pais[0]} y posee un total de {int(confirmados[0])} casos confirmados de COVID-19, teniendo una poblacion de {int(poblacion[0])} habitantes\n"
             f"El segundo pais que mas consume carne con {int(carne[1])} Kcal es {pais[1]} y posee un total de {int(confirmados[1])} casos confirmados de COVID-19, teniendo una poblacion de {int(poblacion[1])} habitantes\n"
             f"El tercer pais que mas consume carne con {int(carne[2])} Kcal es {pais[2]} y posee un total de {int(confirmados[2])} casos confirmados de COVID-19, teniendo una poblacion de {int(poblacion[2])} habitantes\n"
             f"El cuarto pais que mas consume carne con {int(carne[3])} Kcal es {pais[3]} y posee un total de {int(confirmados[3])} casos confirmados de COVID-19, teniendo una poblacion de {int(poblacion[3])} habitantes\n"
             f"El quinto pais que mas consume carne con {int(carne[4])} Kcal es {pais[4]} y posee un total de {int(confirmados[4])} casos confirmados de COVID-19, teniendo una poblacion de {int(poblacion[4])} habitantes\n")
    
    conclusion = ("Podemos concluir, en base a la informacion extraida del data set, que el consumo de carne no se relaciona con el aumento de los casos confirmados de COVID-19.\n")
    
    return pais, confirmados, carne, resp1, data_1, conclusion

def pregunta_2(data):
    data_2 = data.loc[:, ["Country","Alcoholic Beverages","Obesity","Population"]]
    data_2["Obesity"] = clear_variables(data_2["Obesity"])
    data_2 = data_2.sort_values("Alcoholic Beverages", ascending = False)
    data_2 = data_2.iloc[[0,1,2,3,4]]
    pais = data_2["Country"].to_list()
    alcohol = data_2["Alcoholic Beverages"].to_list()
    obesidad = data_2["Obesity"].to_list()
    obesidad = data_2["Population"].to_list()
    
    resp2 = (f"El pais que mas consume alcohol con {int(alcohol[0])} Kcal es {pais[0]}, presentando un indice de obesidad del {int(obesidad[0])}%\n"
             f"El segundo pais que mas consume alcohol con {int(alcohol[1])} Kcal es {pais[1]}, presentando un indice de obesidad del {int(obesidad[1])}%\n"
             f"El tercer pais que mas consume alcohol con {int(alcohol[2])} Kcal es {pais[2]}, presentando un indice de obesidad del {int(obesidad[2])}%\n"
             f"El cuarto pais que mas consume alcohol con {int(alcohol[3])} Kcal es {pais[3]}, presentando un indice de obesidad del {int(obesidad[3])}%\n"
             f"El quinto pais que mas consume alcohol con {int(alcohol[4])} Kcal es {pais[4]}, presentando un indice de obesidad del {int(obesidad[4])}%\n")
    
    conclusion = ("Podemos concluir, en base a la informacion obtenida, que el consumo de alcohol no es la causa principal en el aumento de la tasa de obesidad.\n")    
    
    return pais, alcohol, obesidad, resp2, data_2, conclusion

def pregunta_3(data):
    data_3 = data.loc[:, ["Country","Obesity","Deaths","Population"]]
    data_3["Obesity"] = clear_variables(data_3["Obesity"])
    data_3["Deaths"] = clear_variables(data_3["Deaths"])
    data_3["Obesity"] = (data_3["Population"] / 100) * data_3["Obesity"]
    data_3["Deaths"] = (data_3["Population"] / 100) * data_3["Deaths"]
    data_3 = data_3.sort_values("Obesity", ascending = False)
    data_3 = data_3.iloc[[0,1,2,3,4]]
    pais = data_3["Country"].to_list()
    obesidad = data_3["Obesity"].to_list()
    poblacion = data_3["Population"].to_list()
    muertes = data_3["Deaths"].to_list()

    resp3 = (f"El pais con mayor prevalencia de personas obesas es {pais[0]}, con un total de {int(obesidad[0])}, teniendo una poblacion de {int(poblacion[0])} habitantes\n"
             f"El segundo pais con mayor prevalencia de personas obesas es {pais[1]}, con un total de {int(obesidad[1])}, teniendo una poblacion de {int(poblacion[1])} habitantes\n"
             f"El tercer pais con mayor prevalencia de personas obesas es {pais[2]}, con un total de {int(obesidad[2])}, teniendo una poblacion de {int(poblacion[2])} habitantes\n"
             f"El cuarto pais con mayor prevalencia de personas obesas es {pais[3]}, con un total de {int(obesidad[3])}, teniendo una poblacion de {int(poblacion[3])} habitantes\n"
             f"El quinto pais con mayor prevalencia de personas obesas es {pais[4]}, con un total de {int(obesidad[4])}, teniendo una poblacion de {int(poblacion[4])} habitantes\n")
    
    conclusion = ("Podemos concluir, en base a los resultados obtenidos, que las tasas de obesidad altas se encuentran en paises grandes (de alta extension geografica).\n")     
    
    return pais, obesidad, muertes, resp3, data_3, conclusion

def pregunta_4(data):
    pais, obesidad, muertes, resp4, data_4, conclusion = pregunta_3(data)
    data_4 = data_4.sort_values("Deaths", ascending = False)
    pais = data_4["Country"].to_list()
    obesidad = data_4["Obesity"].to_list()
    poblacion = data_4["Population"].to_list()
    muerte = data_4["Deaths"].to_list()

    resp4 = (f"El pais con mayor prevalencia de personas obesas ordenado por su mortandad es {pais[0]} con un total de {int(muerte[0])} muertes, teniendo una poblacion de {int(poblacion[0])} habitantes\n"
             f"El segundo pais con mayor prevalencia de personas obesas ordenado por su mortandad es {pais[1]}, con un total de {int(muerte[1])} muertes, teniendo una poblacion de {int(poblacion[1])} habitantes\n"
             f"El tercer pais con mayor prevalencia de personas obesas ordenado por su mortandad es {pais[2]}, con un total de {int(muerte[2])} muertes, teniendo una poblacion de {int(poblacion[2])} habitantes\n"
             f"El cuerto pais con mayor prevalencia de personas obesas ordenado por su mortandad es {pais[3]}, con un total de {int(muerte[3])} muertes, teniendo una poblacion de {int(poblacion[3])} habitantes\n"
             f"El quinto pais con mayor prevalencia de personas obesas ordenado por su mortandad es {pais[4]}, con un total de {int(muerte[4])} muertes, teniendo una poblacion de {int(poblacion[4])} habitantes\n")
    
    conclusion = ("Podemos concluir, en base a los datos obtenidos, que las tasas de obesidad y muerte altas se encuentran en paises de gran extension territorial y con gran cantidad de ciudadanos")
                 
    return pais, obesidad, muertes, resp4, data_4, conclusion

def pregunta_5(data):
    data_5 = data.loc[:, ["Country","Alcoholic Beverages","Confirmed","Population"]]
    data_5["Confirmed"] = clear_variables(data_5["Confirmed"])
    data_5 = data_5.sort_values("Alcoholic Beverages", ascending = False)
    data_5 = data_5.iloc[[0,1,2,3,4,5,6,7,8,9]]
    data_5["Confirmed"] = (data_5["Population"]/100)*data_5["Confirmed"]
    poblacion = data_5["Population"].to_list()
    pais = data_5["Country"].to_list()
    alcohol = data_5["Alcoholic Beverages"].to_list()
    confirmados = data_5["Confirmed"].to_list()
    resp5 = (f"El pais que mas consume alcohol con {int(alcohol[0])} Kcal es {pais[0]} con {int(confirmados[0])} casos de COVID-19 confirmados, teniendo una población de {int(poblacion[0])} habitantes\n"
             f"El segundo pais que mas consume alcohol con {int(alcohol[1])} Kcal es {pais[1]} con {int(confirmados[1])} casos de COVID-19 confirmados, teniendo una población de {int(poblacion[1])} habitantes\n"
             f"El terecer pais que mas consume alcohol con {int(alcohol[2])} Kcal es {pais[2]} con {int(confirmados[2])} casos de COVID-19 confirmados, teniendo una población de {int(poblacion[2])} habitantes\n"
             f"El cuarto pais que mas consume alcohol con {int(alcohol[3])} Kcal es {pais[3]} con {int(confirmados[3])} casos de COVID-19 confirmados, teniendo una población de {int(poblacion[3])} habitantes\n"
             f"El quinto pais que mas consume alcohol con {int(alcohol[4])} Kcal es {pais[4]} con {int(confirmados[4])} casos de COVID-19 confirmados, teniendo una población de {int(poblacion[4])} habitantes\n")

    conclusion = ("Como podemos apreciar con los resultados obtenidos, el pais que tiene el mayor consumo de alcohol posee menos casos activos que el octavo pais que mas consume, ademas, no\n se repite ningun patron"
                  "como que los casos activos aumenten descendiente o relativamente descendientemente, por lo cual no es posible afirmar la hipotesis de que los paises\n que mas consumen alcohol son los que mas casos"
                  "activos tienen.")

    return pais, alcohol, confirmados, resp5, data_5, conclusion
