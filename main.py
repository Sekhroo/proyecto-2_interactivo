#!/usr/bin/env python3
#-*- coding:utf-8 -*-
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from window_info import Window_Info
import prueba

class MainWindow():
    def __init__(self):
        self.new_data = prueba.modifica_data()

        builder = Gtk.Builder()
        builder.add_from_file("./def_windows/window_main.ui")
        
        interfaz = builder.get_object("window_main")
        interfaz.connect("destroy", Gtk.main_quit)
        interfaz.set_title("Analisis DataBase")
        interfaz.maximize()

        self.combobox_qstion = builder.get_object("qstion_cbb")
        render_text = Gtk.CellRendererText()
        self.combobox_qstion.pack_start(render_text, True)
        self.combobox_qstion.add_attribute(render_text, "text", 0)
        qstion = Gtk.ListStore(str)

        self.combobox_qstion.connect("changed", self.on_cbb_qstion_select)

        preguntas = ["¿Cuáles son los países con mayor consumo de carne y que relación tiene con sus casos confirmados de COVID-19?",
                     "¿Cuáles son los países con mayor consumo de alcohol y que relación tiene con su índice de obesidad?",
                     "¿Cuáles son los países que registran la mayor cantidad de habitantes obesos?", 
                     "¿Cuáles son los 5 paı́ses que tienen más habitantes con obesidad y que además lo que más muertes tienen?",
                     "¿Los paı́ses que tienen mayor consumo de alcohol son los paı́ses que más contagios confirmados tienen?"]
        
        for i in preguntas:
            qstion.append([i])

        self.combobox_qstion.set_model(qstion)

        btn_acerca_de = builder.get_object("btn_info")
        btn_acerca_de.connect("clicked", self.on_btn_info)

        self.graf = builder.get_object("graf_imag")
        self.datos_tabla = builder.get_object("datos_pandas")
        self.respuesta = builder.get_object("resp_id")
        self.variables = builder.get_object("var_id")
        self.datos = builder.get_object("dat_id")
        self.conclusiones = builder.get_object("conclusion_f")

        interfaz.show_all()

    def on_btn_info(self, btn=None):
        w_inf = Window_Info()
        resp = w_inf.window_info.run()

        if resp == Gtk.ResponseType.CANCEL:
            w_inf.window_info.destroy()

    def on_cbb_qstion_select(self, cmb=None):
        lista = Gtk.ListStore(str, str, str, str)
        self.datos_tabla.set_model(model=lista)

        cell = Gtk.CellRendererText()

        it = self.combobox_qstion.get_active_iter()
        modelo = self.combobox_qstion.get_model()
        self.valor_combobox = modelo[it][0]

        if self.valor_combobox == "¿Cuáles son los países con mayor consumo de carne y que relación tiene con sus casos confirmados de COVID-19?":
            paises, confirmados, carne, resp, variable, conclusion = prueba.pregunta_1(self.new_data)
            self.graf.set_from_file("./graf_windows/G1_P1.png")
        elif self.valor_combobox == "¿Cuáles son los países con mayor consumo de alcohol y que relación tiene con su índice de obesidad?":
            paises, alcohol, obesidad, resp, variable, conclusion = prueba.pregunta_2(self.new_data)
            self.graf.set_from_file("./graf_windows/G2_P2.png")
        elif self.valor_combobox == "¿Cuáles son los países que registran la mayor cantidad de habitantes obesos?":
            paises, obesidad, muerte, resp, variable, conclusion = prueba.pregunta_3(self.new_data)
            self.graf.set_from_file("./graf_windows/G3_P3.png")
        elif self.valor_combobox == "¿Cuáles son los 5 paı́ses que tienen más habitantes con obesidad y que además lo que más muertes tienen?":
            paises, obesidad, muerte, resp, variable, conclusion = prueba.pregunta_4(self.new_data)
            self.graf.set_from_file("./graf_windows/G4_P4.png")
        elif self.valor_combobox == "¿Los paı́ses que tienen mayor consumo de alcohol son los paı́ses que más contagios confirmados tienen?":
            paises, alcohol, confirmados, resp, variable, conclusion = prueba.pregunta_5(self.new_data)
            self.graf.set_from_file("./graf_windows/G5_P5.png")
        if self.datos_tabla.get_columns():
            for column in self.datos_tabla.get_columns():
                self.datos_tabla.remove_column(column)

        for value in variable.values:
            row = [str(i) for i in value]
            lista.append(row)
        for i in range(4):
            column = Gtk.TreeViewColumn(variable.columns[i],
                                        cell,
                                        text=i)
            self.datos_tabla.append_column(column)
        self.respuesta.set_text(resp)
        self.conclusiones.set_text(conclusion)

if __name__ == "__main__":
    MainWindow()
    Gtk.main()
